Forms
=====

Ferris provides HTML form support using the `wtforms <http://wtforms.simplecodes.com/docs/1.0.2/>`_ library. Please consult the wtforms documentation for a list of field types and advanced form usage. This documentation covers how to use wtforms in conjuction with Ferris.

.. module:: ferris.core.forms


Model Form
----------

Usually you'll be using forms to interact with model data. This sort of use is called a *Model Form*, a model can have any number of forms (for example, different forms for different workflow steps).

You can generate a model form automatically using :func:`model_form`:

.. autofunction:: model_form

For example::

    CatInfoForm = model_form(Cat)


This is exactly what :doc:`scaffolding` does if you do not explicitly specify a model form.

You can also add additional fields to your form just like any other form::

    class CatInfoForm(model_form(Cat)):
        claws = wtforms.fields.BooleanField()

.. tip::

    All of the standard arguments to model_form work, including ``only``, ``exclude``, and most importantly ``field_args``. Check out the `associated documentation for wtforms <http://wtforms.readthedocs.org/en/latest/ext.html#ndb>`_.

When using :doc:`scaffolding`, you can specify the Model Form to use by setting :attr:`Scaffold.ModelForm` or ``self.scaffold.ModelForm``.

For example::

    def Cats(Controller):
        class Meta:
            ...

        Class Scaffold:
            ModelForm = CatInfoForm

or::

    def add(self):
        self.scaffold.ModelForm = CatInfoForm
        return scaffold.add(self)


Using Forms in Controller
-------------------------

Once you have a form class, you can process form data into your form using :doc:`request_parsers`.

For example::

    def contact_us(self):
        form = ContactUsForm()
        self.parse_request(container=form)

        if self.request.method != 'GET' and form.validate():
            return form.message


Using Forms in Views
--------------------

.. _form_macros:

Since Ferris uses Jinja2 and wtforms, all of the same principles in the wtforms documentation apply.

Ferris does however provide one useful macro in ``macros/form.html``:

.. method:: FormMacros.form_field(form, field, container_element='div')

    Generates a bootstrap style form control with error message and help block.

For example::

    {% import "macros/form.html" as f with context %}

    <form>
        {{f.form_field(form, form.title)}}
        {{f.form_field(form, form.description)}}
    </form>
