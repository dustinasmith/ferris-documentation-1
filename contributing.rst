Contributing & License
======================

Ferris is open-source software under the `Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>`_ and is copywrite 2013 by Cloud Sherpas and Jonathan Parrott.

We welcome any bug reports and feature requests via the public `bitbucket issue tracker <https://bitbucket.org/cloudsherpas/ferris-framework/issues>`_.

We also welcome any pull requests and will work with you to integrate them into the codebase if they don't exactly match up. If you're planning on doing something big `let us know <https://groups.google.com/forum/?fromgroups#!forum/ferris-framework>`_ and we can give you some guidance and help and additionally make sure we're not duplicating effort.

This documentation has a separate repository and issue tracker and we again welcome issues, requests, and pull requests via `bitbucket <https://bitbucket.org/cloudsherpas/ferris-documentation>`_. 

The `Ferris Google Group <https://groups.google.com/forum/?fromgroups#!forum/ferris-framework>`_ is a great place to ask questions and interact with other developers using Ferris. While we currently do not actively watch StackOverflow we will happily answer questions there if linked to the group.
